#!/usr/bin/perl
use strict;
use autodie;
use Getopt::Long;
my $h='';
my $out_dir='';
my $mapping_dir='';
GetOptions ('help' => \$h, 'h' => \$h, 'm=s'=>\$mapping_dir, 'o=s'=>\$out_dir);
if ($h==1 || $mapping_dir eq "" || $out_dir eq ""){
	print "# Script to parse the mapping of mock communities against reference viral genomes
# Arguments : 
# -m : bam files directory
# -o : output directory\n";
	die "\n";	
}

my $ref_file="Files/Mock_communities_reads_bp_raw_qc.tsv";
my $db_length="Files/Ref_database_length.tsv";
my $path_to_python_script="Mock_communities_get_length_coverage.py";
my $samtools_path="samtools";
my $tmp_file="tmp_file.tsv";
if (-e $tmp_file){
	`rm $tmp_file`;
}
my $tmp_file_result="tmp_file_result.csv";
if (-e $tmp_file_result){
	`rm $tmp_file_result`;
}
my $factor=1000000000; # 10^9 to transform the coverage per bp in coverage per kb per Mb of metagenome

open my $t,"<",$ref_file;
my %store_nb_bp;
my %store_nb_reads;
while(<$t>){
	chomp($_);
	if ($_=~/^##/){next;}
	if ($_ eq ""){next;}
	my @tab=split("\t",$_);
	my @t2=split("_",$tab[0]);
	$tab[0]=$t2[0];
	print "$tab[0]\n";
	$store_nb_bp{$tab[0]}=$tab[4];
	$store_nb_reads{$tab[0]}=$tab[3];
}
close $t;
print "nb reads and bp taken\n";

my %store_db_seq_len;
open my $tsv,"<",$db_length;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$store_db_seq_len{$tab[0]}=$tab[1];
}
close $tsv;
print "length of db sequences taken\n";


my @list_samples=sort keys %store_nb_bp;

my $th_id=100; # We only look at 100% identity matches
foreach(@list_samples){
	my $id_sample=$_;
	print "### Computing coverage for $id_sample ... \n";
	my $out_file=$out_dir.$id_sample."_contig_coverage.csv";
	if (-e $out_file){print "$out_file already here, we skip\n";next;}
	my %store_coords=();
	my %store_total=();
	my %store_total_cover=();
	my $id_sample_for_looking=$id_sample;
	my @list_mapped=<$mapping_dir/$id_sample*.bam>;
	my $c_reads;
	open my $tmp_out,">",$tmp_file;
	foreach(@list_mapped){
		my $file=$_;
		print "   Parsing $file\n";
		open(BAM,"$samtools_path view $file |") || die ("pblm opening file $samtools_path / $file \n");
		while(<BAM>){
			chomp($_);
			if ($_=~/^@/){}
			else{
				$c_reads++;
				my @tab=split("\t",$_);
				my $start=$tab[3];
				my $end=$start+length($tab[9]);
				if (!defined($store_db_seq_len{$tab[2]})){
					print "ignoring $tab[2]\n";
				}
				else{
					my $match=0;
					my $mismatch=0;
					my $md_index=11;
					for (my $i=11;$i<=$#tab;$i++){
						if ($tab[$i]=~/^MD/){$md_index=$i;}
					}
					$tab[$md_index]=~/MD:[^:]+:(.*)/;
					$tab[$md_index]=$1;
					while (length($tab[$md_index])>0){
# 						print "$tab[$md_index]\n";
						if ($tab[$md_index]=~/^\D/){
							while($tab[$md_index]=~/^\D(.*)/){
								$tab[$md_index]=$1;
								$mismatch++;
# 								print "\tone mismatch -> $mismatch == $tab[$md_index]\n";
							}
						}
						elsif ($tab[$md_index]=~/^(\d+)(.*)/){
							$match+=$1;
							$tab[$md_index]=$2;
# 							print "\t$1 matches -> $match == $tab[$md_index]\n";
						}
						else{
							print "I can't deal with $tab[$md_index] !!! - $_\n";
							<STDIN>;
						}
					}
					my @tab=split("\t",$_);
					my $ali_length=$match+$mismatch;
					if (length($tab[9])>$ali_length){$ali_length=length($tab[9]);}
					my $id=int ($match/($match+$mismatch)*10000)/100;
					if ($id>=$th_id){
						$store_total{$tab[2]}++;
						$store_total_cover{$tab[2]}+=length($tab[9]);
						print $tmp_out "$tab[2]\t$start\t$end\n";
					}
				}
			}
		}
		close BAM;
	}
	close $tmp_out;
	`python $path_to_python_script`;
	my %store_cover;
	open my $csv,"<",$tmp_file_result;
	while(<$csv>){
		chomp($_);
		my @tab=split(",",$_);
		$store_cover{$tab[0]}=$tab[1];
	}
	close $csv;
	my $pcent=int($c_reads/$store_nb_reads{$id_sample}*10000)/100;
	print "$c_reads mapped / $store_nb_reads{$id_sample} total / $pcent% reads mapped\n";
	print "We have all the coverage, now compute the actual numbers\n";
	my $total_bp=$store_nb_bp{$id_sample};
	if ($total_bp==0){print "!!! no bp for $id_sample ???\n";<STDIN>;}
	open my $s1,">",$out_file;
	print $s1 "## Contig_Id,Length,Nb_reads_Mapped,Coverage,Norm_coverage,Length_covered\n";
	foreach(sort {$store_total{$b} <=> $store_total{$a} } keys %store_total){
		my $id_genome=$_;
		if ($store_db_seq_len{$id_genome}==0){
			print "no length for $id_genome???\n";
			<STDIN>;
		}
		my $length_covered=$store_cover{$id_genome};
		my $cover=$store_total_cover{$id_genome};
		$cover/=$store_db_seq_len{$id_genome};
		my $norm_cover=$cover/$total_bp*$factor;
		print $s1 "$id_genome,$store_db_seq_len{$id_genome},$store_total{$id_genome},$cover,$norm_cover,$length_covered\n";
	}
	close $s1;
	print "$id_sample processed\n";
}
