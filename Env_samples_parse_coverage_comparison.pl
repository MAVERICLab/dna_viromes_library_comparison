#!/usr/bin/perl
use strict;
# use warnings;
use autodie;
use Getopt::Long;
my $h=0;
my $code='';
GetOptions ('help' => \$h, 'h' => \$h, 'c=s'=>\$code);
if ($h==1 || $code eq ""){ # If asked for help or did not set up any argument
	print "# Script to parse the coverage of viral contigs between MDA vs Swift vs Nextera
# Arguments 
-c : code of the dataset to process (format Sample_XX)
\n";
	die "\n";
}

# Sample_102S

my $dataset_size="Files/Nb_reads_bp_qc_by_sample.tsv";
my $anot_dir="Files/Annotation/";
my $cover_dir="Files/Coverage_scaffolds/";
my $out_file_matrix="Comparison_".$code.".csv";


my $added="";
if ($code=~/^Sample/){$added="Pool_";}
my $viral_list=$anot_dir.$added.$code."_list_viral_contigs.tsv";
my $file_annot=$anot_dir.$added.$code."_affi-contigs.csv";
my $file_conta=$anot_dir.$added.$code."_list_conta.tsv";

my %exclude=("Ham1p_like"=>1,"2OG-FeII_Oxy_2"=>1,"MreB_Mbl"=>1,"FtsJ"=>1,"YP_009067064.1"=>1,"DEAD"=>1,"YP_173236.1"=>1,"AAA_11"=>1); # Domains you find in some RNA viruses but also found in a lot of other virus and cellular genomes, so we don't consider these for contig affiliation

# Taking nber of bp sequenced in each dataset
my %info;
open my $tsv,"<",$dataset_size;
while(<$tsv>){
	chomp($_);
	if ($_=~/^##/){next;}
	my @tab=split("\t",$_);
	if ($tab[0]=~/^([^-]+)-([GNS])/){
		my $sample_code=$1."-".$2;
# 		print "$tab[0] -> $sample_code -> + $tab[2]\n";
		$info{$sample_code}{"n_bp"}+=$tab[2];
	}
	else{
		print "$tab[0] ?\n";
		<STDIN>;
	}
}
close $tsv;


# Checking the viral contigs
my %check_virus;
open my $csv,"<",$viral_list;
my $tag=0;
while(<$csv>){
	chomp($_);
	$check_virus{$_}=1;
}
close $csv;

# Checking the lab contamination
my %check_conta;
open my $tsv,"<",$file_conta;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$check_conta{$tab[0]}=1;
}
close $tsv;

# Taking scaffolds annotation
open my $csv,"<",$file_annot;
my %count;
my %info_scaf;
my $c_c="";
my $tag=0;
my %affi_scaf;
while(<$csv>){
	chomp($_);
	if ($_=~/^>(.*)/){
		my @tab=split(/\|/,$1);
		$tab[0]=~s/-circular//;
		$tag=0;
		if ($check_virus{$tab[0]}==1 && !($check_conta{$tab[0]}==1)){
			$tag=1;
			$info_scaf{$tab[0]}{"nb_genes"}=$tab[1];
			$info_scaf{$tab[0]}{"type"}=$tab[2];
			$c_c=$tab[0];
		}
	}
	elsif($tag==1){
		my @tab=split(/\|/,$_);
		if ($tab[6]>50 && $tab[8] ne "-"){
			if ($tab[8]=~/dsDNA/){$count{$c_c}{"dsDNA"}++;}
			elsif($tab[8]=~/ssDNA/){
				if ($tab[8]=~/Cellulophaga/){}
				elsif($tab[9]=~/Resolvase/){} # this is to not consider ssDNA everything that matches the Inoviridae resolvase, because these are also found in some dsDNA
				else{
					$count{$c_c}{"ssDNA"}++;
					my @t2=split(";",$tab[8]);
					$affi_scaf{$c_c}.="$t2[2]-$t2[3];";
				}
			}
			elsif($tab[8]=~/RNA/ && (!($exclude{$tab[9]}==1)) && (!($exclude{$tab[5]}==1))){
				$count{$c_c}{"RNA"}++;
				print "$_\n";
				if ($tab[10]/$tab[6]>1.5){
					print "!! Sure about this one ???\n";
				}
			}
			else{
				$count{$c_c}{"dsDNA"}++;
			}
		}
	}
}
close $csv;


my %count_types;
foreach my $scaf (sort keys %info_scaf){
	if (!defined($count{$scaf})){
		$info_scaf{$scaf}{"affi"}="unknown";
		$count_types{"unknown"}++;
	}
	else{
		my @tab=keys %{$count{$scaf}};
		if ($#tab==0){
			$info_scaf{$scaf}{"affi"}=$tab[0];
			$count_types{$tab[0]}++;
		}
		else{
			print "$scaf is mixed\n";
			$info_scaf{$scaf}{"affi"}="unknown";
			$count_types{"unknown"}++;
		}
	}
}
print "####\n";
foreach my $cat (sort keys %count_types){print "$cat\t$count_types{$cat}\n";}

# Now getting the coverage values
my $th_covered=50; # For these Swift cross-assemblies, we use 50% minimum for a contig to be detected in a virome, to allow for contigs assembled from reads originating from a combination of these different datasets to still be picked up

my %store;
my %store_invert;
my $code_bis=$code;
$code_bis=~s/^Sample_//;
my @tab_coverage=<$cover_dir/$code_bis*.csv>;
my @tab_samples;
foreach my $file (@tab_coverage){
	$file=~/.*\/([^\/]*)_sorted_coverage\.csv/;
	my $sample="Sample_".$1;
	if (!defined($info{$sample})){
		print "!! no info $sample\n";
		<STDIN>;
	}
	print "Processing $file -> $sample == $info{$sample}{n_bp}\n";
	my $factor=1000000000/$info{$sample}{"n_bp"};
	push(@tab_samples,$sample);
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		my @tab=split(",",$_);
		if ($tab[1] eq "length"){}
		elsif ($check_conta{$tab[0]}==1){
			print "$tab[0] was identified as lab contamination, we don't care\n";
		}
		elsif ($tab[2]>=$th_covered){
			my $cover=$tab[3]/$tab[1];
			$tab[0]=~s/\./_/g;
			$tab[0]=$added.$code."_".$tab[0];
			if ($check_virus{$tab[0]}==1 && !($check_conta{$tab[0]}==1)){
				$store{$tab[0]}{$sample}=$cover;
				$store_invert{$sample}{$tab[0]}=$cover;
			}
		}
	}
	close $csv;
}

# Now trying to guess the affiliation of scaffolds based on differential coverage
my %count;
open my $s1,">",$out_file_matrix;
print $s1 "Scaffold,".join(",",@tab_samples);
foreach my $scaf (sort keys %info_scaf){
	if (!defined($store{$scaf})){
		next;
	}
	my %cover=%{$store{$scaf}};
	my @tab=keys %cover;
	my $next=$code."-N";
	if($info_scaf{$scaf}{"affi"} eq "dsDNA"){
		# nothing to do here
	}
	elsif($info_scaf{$scaf}{"affi"} eq "ssDNA"){
		if (defined($cover{$next})){
			print "!! $scaf is covered in Nextera sample when it should be a ssDNA virus ?\n";
		}
	}
	else{
		if (defined($cover{$next})){
			# Found in nextera, probably dsDNA
			$info_scaf{$scaf}{"affi"}="dsDNA_predicted";
			$count_types{"dsDNA_predicted"}++;
		}
		else{
			# Only Swift / Genomiphi, probably ssDNA
			$info_scaf{$scaf}{"affi"}="ssDNA_predicted";
			$count_types{"ssDNA_predicted"}++;
		}
	}
	my $l=$scaf;
	foreach my $s (@tab_samples){
		if (defined($cover{$s})){
			$l.=",".$cover{$s};
			$count{$info_scaf{$scaf}{"affi"}}{$s}+=$cover{$s};
		}
		else{$l.=",0";}
	}
	print $s1 "$l\n";
}
close $s1;


print "####\n";
foreach my $cat (sort keys %count_types){print "$cat\t$count_types{$cat}\n";}

print "### Sample\tmax affiliated\t max all ssDNA\n";
foreach my $sample (sort keys %store_invert){
	my $c_ss=0;
	foreach my $contig (keys %{$store_invert{$sample}}){
		if ($info_scaf{$contig}{"affi"} eq "ssDNA" || $info_scaf{$contig}{"affi"} eq "ssDNA_predicted"){
			
		}
		else{
			$store_invert{$sample}{$contig}/=2;
		}
	}
	my @tab=sort {$store_invert{$sample}{$b}<=>$store_invert{$sample}{$a}} keys %{$store_invert{$sample}};
	my $max_all_ssDNA=-1;
	my $max_affiliated_ssDNA=-1;
	for (my $i=0;$i<=$#tab;$i++){
		if ($info_scaf{$tab[$i]}{"affi"} eq "ssDNA" && $max_affiliated_ssDNA==-1){
			print "known ssDNA rank $i -> $tab[$i] -> $store{$tab[$i]}{$sample}\n";
		}
		if (($info_scaf{$tab[$i]}{"affi"} eq "ssDNA_predicted" || $info_scaf{$tab[$i]}{"affi"} eq "ssDNA") && $max_all_ssDNA==-1){
			$max_all_ssDNA=$i+1;
			print "known or predicted ssDNA rank $i -> $tab[$i]\n";
		}
		if ($max_affiliated_ssDNA>-1 && $max_all_ssDNA>-1){last;}
	}
	print "$sample\t$max_affiliated_ssDNA\t$max_all_ssDNA\n";
}

print "Group,".join(",",@tab_samples)."\n";;
foreach my $g (sort keys %count){
	my $l=$g;
	foreach my $s (@tab_samples){
		if (defined($count{$g}{$s})){$l.=",".$count{$g}{$s}}
		else{$l.=",0";}
	}
	print "$l\n";
}
