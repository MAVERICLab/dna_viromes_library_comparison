### What is this repository for? ###

This repository documents the different scripts used in the analysis of dsDNA-ssDNA viromes, reported in the manuscript "Towards quantitative viromics for both double-stranded and single-stranded DNA viruses".