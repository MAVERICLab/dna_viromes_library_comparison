import logging
import fnmatch
import os
import pandas 
import numpy as np
from glob import glob

def coverage_vectors(contigs_size):
    coverage = {}
    for i in contigs_size.index:
        coverage[contigs_size.ix[i,0]] = np.zeros(contigs_size.ix[i,1])
    return coverage

def parse(fi,coverage):
    with open(fi,"r") as f:
        for i,l in enumerate(f):
            try:
                l = l.split()
                coverage[l[0]][int(l[1]):int(l[2])] = 1
            except Exception:
                print "file: {0} line: {1}".format(fi,i)
                print l
                raise
    return coverage

def main():
    fi="tmp_file.tsv"
    outfi="tmp_file_result.csv"
    # Create empty vectors
    print("Reading the table of sequences and their length")
    contigs_size = pandas.read_table("Ref_database_length.tsv",header=None)
    print("Getting the coverage tables ready")
    coverage = coverage_vectors(contigs_size)
    print("Getting coverage values")
    coverage = parse(fi,coverage)
    print("Computing coverage proportion.")
    coverage_prop = {}
    for contig,vector in coverage.items():
        coverage_prop[contig] = np.sum(vector)/float(len(vector))
    print("Now writing the output")
    output=pandas.Series(coverage_prop)
    output.to_csv(outfi)
    print "{0} file wrote ({1} entries)".format(outfi,output.shape)
    return output

if __name__ == "__main__":
    output = main()
